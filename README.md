# Related Items

## Overview

In this project we will be using a technique named **Item2Vec** which is an adaptation of the well known technique **Word2Vec**.
    
**Word2Vec** is an algorithm that uses a neural network to learn associations from a large corpus of text.

Our algorithm **Item2Vec** works mainly in the following way:

* It converts each distinct item to a particular list of numbers called a *vector* considering the related hierarchy and basket information as sentences.
* These vectors are chosen carefully such that the similarity (or distance) metric of the vectors indicates the semantic similarity between the items represented by the vectors.
* These similarity metrics are often simple numerical functions such as *Cosine Similarity*, etc.


## Getting Started, example

Here is an example of how **Item2Vec** works in PySpark to find item recommendations.

### Filtering Data

We will filter out the baskets based on a recent particular timeframe or a fixed number of last transactions of the customers. 


### Creating Item Embeddings

We will find the item embeddings for all the items using **Item2Vec**.

```
from pyspark.ml.feature import Word2Vec

sent = ("Toffee Candy Lollypop " * 100 + "Marshmallow Candy Jelly " * 10).split(" ")
doc = spark.createDataFrame([(sent,), (sent,)], ["sentence"])
word2Vec = Word2Vec(vectorSize=5, seed=42, inputCol="sentence", outputCol="model")
model = word2Vec.fit(doc)
model.getVectors().show()
```
**Output:** 
```
+-----------+--------------------+
|       word|              vector|
+-----------+--------------------+
|Marshmallow|[0.58746010065078...|
|     Toffee|[-0.9246177673339...|
|      Jelly|[0.63015037775039...|
|   Lollypop|[-0.8793326616287...|
|      Candy|[0.03115312010049...|
+-----------+--------------------+
```

### Creating User Embeddings

First we divide the filtered dataframe for each customer into three parts based on the hierarchy of your choice: 1) Top most category, 2) Second most category, 3) Rest of the categories.

Now we obtain the customer embeddings from all three dataframes using weighted average of the item embeddings using frequency and repeat purchase probability as the weights.

Repeat purchase probability for a particular item is defined as the number of customers buying it at least twice divided by the number of customers buying it at least once. 





### Getting Recommendations
 
Now we get the recommendation using the customer embeddings with the help [Faiss](https://faiss.ai/) algorithm after filtering out past items. 
  * If the customer has bought only one category then all 10 recommendations will be from that particular category.
  * If the customer has bought two categories then the number of recommendations will be 5 from both the categories.
  * Otherwise there will be 3 recommendations from top most category, 3 recommendations from second most category and the other 4 recommendations will be from the rest of the categories. 




## Contributors
The following persons have contributed to creating the documentation.

Monosish Kolay, Kushagra Sharma, Arpan Kumar
